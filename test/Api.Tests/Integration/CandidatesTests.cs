using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.TestHost;
using Xunit;

namespace Api.Tests.Integration
{
    public class CandidatesTests
    {
        private readonly TestServer server;
        private readonly HttpClient client;

        public CandidatesTests ()
        {
            this.server = new TestServer(new WebHostBuilder()
                .UseStartup<Startup>());
            this.client = server.CreateClient();
        }

        [Fact]
        public async Task CandidatesReturnsEmptyListWhenNoCandidatesAreAdded()
        {
            // Act
            var response = await this.client.GetAsync("/candidates?name=John");

            // Assert
            var responseString = await response.Content.ReadAsStringAsync();
            Assert.Equal("[]", responseString);
            Assert.Equal(HttpStatusCode.OK, response.StatusCode);
        }

        [Fact]
        public async Task CandidatesReturnsBadRequestWhenDataIsMissing()
        {
            // Arrange
            var formContent = new FormUrlEncodedContent(new[]
            {
                new KeyValuePair<string, string>("name", "John"), 
                new KeyValuePair<string, string>("initials", "jr"),
            });

            // Act            
            var response = await this.client.PostAsync("/candidates", formContent);

            // Assert
            Assert.Equal(HttpStatusCode.BadRequest, response.StatusCode);
        }

        [Fact]
        public async Task CandidatesReturnsAddedCandidates()
        {
            // Arrange
            var formContent = new FormUrlEncodedContent(new[]
            {
                new KeyValuePair<string, string>("name", "John"), 
                new KeyValuePair<string, string>("initials", "jr"),
                new KeyValuePair<string, string>("email", "jr@dr.dk")
            });

            // Act            
            var postResponse = await this.client.PostAsync("/candidates", formContent);
            var getResponse = await this.client.GetAsync("/candidates?name=John");

            // Assert
            var getResponseString = await getResponse.Content.ReadAsStringAsync();
            Assert.Equal(HttpStatusCode.Created, postResponse.StatusCode);
            Assert.Equal(HttpStatusCode.OK, getResponse.StatusCode);
            Assert.Equal(@"[{""name"":""John"",""initials"":""jr"",""email"":""jr@dr.dk""}]", getResponseString);
        }
    }
}
