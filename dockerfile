FROM microsoft/aspnetcore-build:2.0 AS build
WORKDIR /app
COPY ./src/Api/*.csproj .
RUN dotnet restore
COPY ./src/Api/. .
RUN dotnet publish -c Release -o out

FROM microsoft/aspnetcore:2.0
WORKDIR /app
COPY --from=build /app/out .
ENTRYPOINT ["dotnet", "Api.dll"]
