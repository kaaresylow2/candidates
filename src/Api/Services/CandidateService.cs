namespace Api.Services
{
    using Microsoft.Extensions.Caching.Memory;
    using Api.Domain.Models;
    using System.Threading.Tasks;
    using System.Collections.Generic;
    using System.Linq;
    using System.Collections.Concurrent;

    public class CandidateService : ICandidateService
    {
        private readonly ConcurrentBag<Candidate> candidates;

        public CandidateService(IMemoryCache storage)
        {
            this.candidates = new ConcurrentBag<Candidate>();
        }

        public void CreateCandidate(Candidate candidate)
        {
            this.candidates.Add(candidate);
        }

        public IEnumerable<Candidate> GetCandidates(string name)
        {
            if (name == null)
            {
                return candidates;
            }
            
            return this.candidates.Where(n => n.Name == name);
        }
    }
}
