namespace Api.Services
{
    using System.Collections.Generic;
    using Api.Domain.Models;

    public interface ICandidateService
    {
        void CreateCandidate(Candidate candidate);

        IEnumerable<Candidate> GetCandidates(string name);
    }
}