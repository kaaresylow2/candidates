namespace Api.Domain.Models
{
    public class Candidate
    {
        public string Name { get; set; }

        public string Initials { get; set; }

        public string Email { get; set; }
    }
}