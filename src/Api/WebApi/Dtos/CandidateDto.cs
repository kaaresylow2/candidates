namespace Api.WebApi.Dtos
{
    using System.ComponentModel.DataAnnotations;
    using Api.Domain.Models;
    using Microsoft.AspNetCore.Mvc;
    using Newtonsoft.Json;

    public class CandidateDto
    {
        [FromForm]
        [Required]
        public string Name { get; set; }

        [FromForm]
        [Required]
        public string Initials { get; set; }

        [FromForm]
        [Required]
        public string Email { get; set; }

        [JsonConstructor]
        public CandidateDto() {}

        public CandidateDto(string name, string initials, string email)
        {
            this.Name = name;
            this.Initials = initials;
            this.Email = email;
        }

        public Candidate ToModel() {
            return new Candidate() 
            {   
                Name = this.Name, 
                Initials = this.Initials, 
                Email = this.Email 
            };
        }
    }
}
