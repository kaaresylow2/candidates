namespace Api.WebApi
{
    using System.Collections.Generic;
    using System.Linq;
    using Api.Services;
    using Api.WebApi.Dtos;
    using Microsoft.AspNetCore.Http;
    using Microsoft.AspNetCore.Mvc;

    [Route("[controller]")]
    public class CandidatesController : ControllerBase
    {
        private readonly ICandidateService candidateService;

        public CandidatesController(ICandidateService candidateService)
        {
            this.candidateService = candidateService;
        }

        [HttpGet]
        public IActionResult GetCandidates([FromQuery]string name)
        {   
            var result = this.candidateService
                .GetCandidates(name)
                .Select(c => new CandidateDto(c.Name, c.Initials, c.Email));
            
            return new JsonResult(result);
        }
        
        [HttpPost]
        public IActionResult CreateCandidate(CandidateDto candidate)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest();
            }

            this.candidateService.CreateCandidate(candidate.ToModel());
            
            return StatusCode(StatusCodes.Status201Created);
        }
    }
}
